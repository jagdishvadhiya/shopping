<?php 

class Categorys extends CI_Controller
{
    /**
     * summary
     */
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('aid') && !$this->session->userdata('auname'))
       {
        return redirect('admin/login');

       }
    }
    function index()
    {
        $this->load->model('admin/Admin_model');
        $data=$this->Admin_model->getDataByTable('category');
         $dashboard['categorydata']=$data;
         $dashboard['orders_count']=$this->returncount('orders');
        $dashboard['products_count']=$this->returncount('products');
        $dashboard['category_count']=$this->returncount('category');
        $dashboard['users_count']=$this->returncount('users');
        $dashboard['subcategory_count']=$this->returncount('subcategory');
        $dashboard['productreviews_count']=$this->returncount('productreviews');
       $this->load->view('admin/categorys',$dashboard);
    }
    public function categoryadd()
    {
        if(isset($_POST['addNewCategory']))
        {
             
                  // Form Validation For New User added by Admin
            $this->form_validation->set_rules('cat_name','Category Name','required');
            
            if ($this->form_validation->run()==true)
            {
                // echo "<pre>";
                // print_r($_POST);
                $userdata['categoryName']=$this->input->post('cat_name');
                $userdata['categoryDescription']=$this->input->post('cat_description');
              
                $userdata['creationDate']=date("Y-m-d h:i:s");
                $userdata['status']=$this->input->post('status');
                print_r($_POST);
                $this->load->model('admin/Admin_model');
                if($this->Admin_model->addNewCategory($userdata))
                {
                    $this->session->set_flashdata("add_category_y","Category Added Successfully...");
                    // exit();
                }else
                {
                   $this->session->set_flashdata("add_category_n","Category Not Added Somthing Wrong...");
                    // exit();
                }
                return redirect('admin/categorys');
            }else{
                //form error accurs
                
                $this->form_validation->set_error_delimiters('<p class="error invalid-feedback">','</p>');
                $dashboard['orders_count']=$this->returncount('orders');
                $dashboard['products_count']=$this->returncount('products');
                $dashboard['category_count']=$this->returncount('category');
                $dashboard['users_count']=$this->returncount('users');
                $dashboard['subcategory_count']=$this->returncount('subcategory');
                $dashboard['productreviews_count']=$this->returncount('productreviews');
                $this->load->view('admin/categoryadd',$dashboard);

            }
        }else{

            
             $dashboard['orders_count']=$this->returncount('orders');
            $dashboard['products_count']=$this->returncount('products');
            $dashboard['category_count']=$this->returncount('category');
            $dashboard['users_count']=$this->returncount('users');
            $dashboard['subcategory_count']=$this->returncount('subcategory');
            $dashboard['productreviews_count']=$this->returncount('productreviews');
           $this->load->view('admin/categoryadd',$dashboard);
        }
     
        
    }
    public function editcategory($id)
    {
         $this->load->model('admin/Admin_model');
            $data=$this->Admin_model->getDataById('category',$id);

            $dashboard['editcategorydata']=$data[0];
            $dashboard['orders_count']=$this->returncount('orders');
            $dashboard['products_count']=$this->returncount('products');
            $dashboard['category_count']=$this->returncount('category');
            $dashboard['users_count']=$this->returncount('users');
            $dashboard['subcategory_count']=$this->returncount('subcategory');
            $dashboard['productreviews_count']=$this->returncount('productreviews');
            $this->load->view('admin/editCategory',$dashboard);
        
    }
    function categoryupdate()
    {
         if(isset($_POST['updateCategory']))
        {
             
                  // Form Validation For New User added by Admin
            $this->form_validation->set_rules('cat_name','Category Name','required');
            $id=$this->input->post('cat_id');
            
            if ($this->form_validation->run()==true)
            {
                // echo "<pre>";
                // print_r($_POST);
                $userdata['categoryName']=$this->input->post('cat_name');
                $userdata['categoryDescription']=$this->input->post('cat_description');
              
                $userdata['updationDate']=date("Y-m-d h:i:s");
                $userdata['status']=$this->input->post('status');
                // print_r($userdata);
                $this->load->model('admin/Admin_model');
                if($this->Admin_model->updateCategory($id,$userdata))
                {
                    $this->session->set_flashdata("update_category_y","Category Updated Successfully...");
                    // exit();
                }else
                {
                   $this->session->set_flashdata("update_category_n","Category Not Updated Somthing Wrong...");
                    // exit();
                }
                return redirect('admin/categorys');
            }else{
                //form error accurs
                
                $this->form_validation->set_error_delimiters('<p class="error invalid-feedback">','</p>');
                $this->load->model('admin/Admin_model');
            $data=$this->Admin_model->getDataById('category',$id);

            $dashboard['editcategorydata']=$data[0];
                $dashboard['orders_count']=$this->returncount('orders');
                $dashboard['products_count']=$this->returncount('products');
                $dashboard['category_count']=$this->returncount('category');
                $dashboard['users_count']=$this->returncount('users');
                $dashboard['subcategory_count']=$this->returncount('subcategory');
                $dashboard['productreviews_count']=$this->returncount('productreviews');
                $this->load->view('admin/editcategory',$dashboard);

            }
        }else{

            $this->load->model('admin/Admin_model');
            $data=$this->Admin_model->getDataByTable('category');
             $dashboard['categorydata']=$data;
             $dashboard['orders_count']=$this->returncount('orders');
            $dashboard['products_count']=$this->returncount('products');
            $dashboard['category_count']=$this->returncount('category');
            $dashboard['users_count']=$this->returncount('users');
            $dashboard['subcategory_count']=$this->returncount('subcategory');
            $dashboard['productreviews_count']=$this->returncount('productreviews');
           $this->load->view('admin/categorys',$dashboard);
        }
    }
    function deletecategory($id)
    {
         $this->load->model('admin/Admin_model');
        if($this->Admin_model->deleteCat($id))
                {
                    $this->session->set_flashdata("delete_cat_y","Category Deleted Successfully...");
                    // exit();
                }else
                {
                   $this->session->set_flashdata("delete_cat_n","Category Not Deleted Somthing Wrong...");
                    // exit();
                }
                return redirect('admin/categorys');
    }

    function returncount($table)
    {
        $this->load->model('admin/Admin_model');
        $data=$this->Admin_model->getCountByTable($table);
        return count($data);
        
    }
}