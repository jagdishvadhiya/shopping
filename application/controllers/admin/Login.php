<?php 

/**
 * summary
 */
class Login extends CI_Controller
{
    /**
     * summary
     */
    public function __construct()
    {
    	parent::__construct();
        if($this->session->userdata('aid'))
        {
           
            return redirect('admin/admin/dashboard');
        }
      
    }
    function index()
    {
        if(isset($_POST['adminLogin']))
        {
            
           $this->form_validation->set_rules('email','Email','required');
           $this->form_validation->set_rules('password','Password','required');
           if ($this->form_validation->run()==true) 
           {
               // get value from form input
            // echo password_hash('admin', PASSWORD_DEFAULT);
     
                $email=$this->input->post('email');
                $password=$this->input->post('password');
                $this->load->model('admin/Admin_model');
                $data=$this->Admin_model->getAdminByEmail($email);
                if(!empty($data))
                {
                    // get password from hash
                    if(password_verify($password,$data->password))
                    {
                        $this->session->set_userdata('aid',$data->id);
                        $this->session->set_userdata('auname',$data->username);
                        return redirect('admin/admin');
                    }else{
                         $this->session->set_flashdata('invalidpassword',"Please Enter Valid Password");
                     $this->load->view('admin/login');
                    }
                }else
                {
                    $this->session->set_flashdata('invalidemail',"Please Enter Valid Email");
                     $this->load->view('admin/login');
                }

           }else{
            //error acuurs
            $this->form_validation->set_error_delimiters('<p class="text-danger text-center">','</p>');
            $this->load->view('admin/login');
           }
            
        }else{
            // without form submit redirect to first time login load
        $this->load->view('admin/login');
        }
    }
    function logout()
    {
       $this->session->unset_userdata('aid');
       $this->session->unset_userdata('auname');
       echo "<br>";
       return redirect('admin/admin');

    }
   
}

 ?>