<?php 

class Subcategorys extends CI_Controller
{
    /**
     * summary
     */
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('aid') && !$this->session->userdata('auname'))
       {
        return redirect('admin/login');

       }
    }
    function index()
    {
        $this->load->model('admin/Admin_model');
        $data=$this->Admin_model->getDataByTable('subcategory');
        foreach ($data as $key => $value) {
            $cat_data=$this->Admin_model->getCategoryNameById($data[$key]['categoryid']);
            $data[$key]['categoryName']= $cat_data->categoryName;
        }
        
         $dashboard['subcategorydata']=$data;
         $dashboard['orders_count']=$this->returncount('orders');
        $dashboard['products_count']=$this->returncount('products');
        $dashboard['category_count']=$this->returncount('category');
        $dashboard['users_count']=$this->returncount('users');
        $dashboard['subcategory_count']=$this->returncount('subcategory');
        $dashboard['productreviews_count']=$this->returncount('productreviews');
       $this->load->view('admin/subcategorys',$dashboard);
    }
    public function subcategoryadd()
    {
        if(isset($_POST['addNewSubCategory']))
        {

                  // Form Validation For New User added by Admin
            $this->form_validation->set_rules('sub_cat_name','Category Name','required');
            $cat_id=$this->input->post('category');
            
            if ($this->form_validation->run()==true && $cat_id!=0)
            {
                // echo "<pre>";
                // print_r($_POST);
                $userdata['categoryid']=$this->input->post('category');
                $userdata['subcategory']=$this->input->post('sub_cat_name');
              
                $userdata['creationDate']=date("Y-m-d h:i:s");
                $userdata['status']=$this->input->post('status');
                // print_r($_POST);
                // print_r($userdata);
                $this->load->model('admin/Admin_model');
                if($this->Admin_model->addNewSubCategory($userdata))
                {
                    $this->session->set_flashdata("add_sub_category_y","Sub-Category Added Successfully...");
                    // exit();
                }else
                {
                   $this->session->set_flashdata("add_sub_category_n","Sub-Category Not Added Somthing Wrong...");
                    // exit();
                }
                return redirect('admin/subcategorys');
            }else{
                //form error accurs
                // echo "wrong";
                // echo "<pre>";
                // print_r($_POST);
                // exit();
                
                $this->form_validation->set_error_delimiters('<p class="error invalid-feedback">','</p>');
                $this->session->set_flashdata('selectcat','Please Select Category');
                $dashboard['orders_count']=$this->returncount('orders');
                $dashboard['products_count']=$this->returncount('products');
                $dashboard['category_count']=$this->returncount('category');
                $dashboard['users_count']=$this->returncount('users');
                $dashboard['subcategory_count']=$this->returncount('subcategory');
                $dashboard['productreviews_count']=$this->returncount('productreviews');
                $this->load->view('admin/subcategoryadd',$dashboard);

            }
        }else{

            
             $dashboard['orders_count']=$this->returncount('orders');
            $dashboard['products_count']=$this->returncount('products');
            $dashboard['category_count']=$this->returncount('category');
            $dashboard['users_count']=$this->returncount('users');
            $dashboard['subcategory_count']=$this->returncount('subcategory');
            $dashboard['productreviews_count']=$this->returncount('productreviews');
           $this->load->view('admin/subcategoryadd',$dashboard);
        }
     
        
    }
    public function editsubcategory($id)
    {
         $this->load->model('admin/Admin_model');
            $data=$this->Admin_model->getDataById('subcategory',$id);

            $dashboard['editsubcategorydata']=$data[0];
            $dashboard['orders_count']=$this->returncount('orders');
            $dashboard['products_count']=$this->returncount('products');
            $dashboard['category_count']=$this->returncount('category');
            $dashboard['users_count']=$this->returncount('users');
            $dashboard['subcategory_count']=$this->returncount('subcategory');
            $dashboard['productreviews_count']=$this->returncount('productreviews');
            $this->load->view('admin/editsubCategory',$dashboard);
        
    }
    function subcategoryupdate()
    {
         if(isset($_POST['updateSubCategory']))
        {
             
            
                  // Form Validation For New User added by Admin
           
            $cat_id=$this->input->post('category');
            $sub_cat_name=$this->input->post('sub_cat_name');
            $sub_id=$this->input->post('id');
            
            if ($sub_cat_name!="" && $cat_id!=0)
            {
                echo "<pre>";
                // print_r($_POST);
                $userdata['categoryid']=$this->input->post('category');
                $userdata['subcategory']=$this->input->post('sub_cat_name');
              
                $userdata['updationDate']=date("Y-m-d h:i:s");
                $userdata['status']=$this->input->post('status');
                // print_r($_POST);
                print_r($userdata);
                $this->load->model('admin/Admin_model');
                if($this->Admin_model->updateSubCategory($sub_id,$userdata))
                {
                    $this->session->set_flashdata("update_sub_category_y","Sub-Category Updated Successfully...");
                    // exit();
                }else
                {
                   $this->session->set_flashdata("update_sub_category_n","Sub-Category Not Updated Somthing Wrong...");
                    // exit();
                }
                return redirect('admin/subcategorys');
            }else{
                
                return redirect('admin/subcategorys/editsubcategory/'.$sub_id);
            }
        }else{

          return redirect('admin/subcategorys');
        }
    }
    function deletecategory($id)
    {
         $this->load->model('admin/Admin_model');
        if($this->Admin_model->deleteCat($id))
                {
                    $this->session->set_flashdata("delete_user_y","User Deleted Successfully...");
                    // exit();
                }else
                {
                   $this->session->set_flashdata("delete_user_n","User Not Deleted Somthing Wrong...");
                    // exit();
                }
                return redirect('admin/categorys');
    }
     function deletesubcategory($id)
    {
         $this->load->model('admin/Admin_model');
        if($this->Admin_model->deleteSubCat($id))
                {
                    $this->session->set_flashdata("delete_subcategory_y","Sub-category Deleted Successfully...");
                    // exit();
                }else
                {
                   $this->session->set_flashdata("delete_subcategory_n","Sub-category Not Deleted Somthing Wrong...");
                    // exit();
                }
                return redirect('admin/subcategorys');
    }

    function returncount($table)
    {
        $this->load->model('admin/Admin_model');
        $data=$this->Admin_model->getCountByTable($table);
        return count($data);
        
    }
}