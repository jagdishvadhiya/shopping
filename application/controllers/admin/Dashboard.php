<?php 

class Dashboard extends CI_Controller
{
    /**
     * summary
     */
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('aid') && !$this->session->userdata('auname'))
       {
        return redirect('admin/login');
        
       }

    }
    function index()
    {
        $dashboard['orders_count']=$this->returncount('orders');
        $dashboard['products_count']=$this->returncount('products');
        $dashboard['category_count']=$this->returncount('category');
        $dashboard['users_count']=$this->returncount('users');
        $dashboard['subcategory_count']=$this->returncount('subcategory');
        $dashboard['productreviews_count']=$this->returncount('productreviews');
       $this->load->view('admin/dashboard',$dashboard);
    }
    
    function returncount($table)
    {
        $this->load->model('admin/Admin_model');
        $data=$this->Admin_model->getCountByTable($table);
        return count($data);
        
    }

    function useradd()
    {
        $this->load->model('admin/Admin_model');
        $totaluser['usercount']=$this->Admin_model->getUsersCount();
        $totaluser['page']="adduser";
        $this->load->view('admin/useradd',$totaluser);   
    }
    function addNewUser()
    {
        if (isset($_POST['addNewUser'])) {
            // Form Validation For New User added by Admin
            $this->form_validation->set_rules('fname','First Name','required');
            $this->form_validation->set_rules('lname','Last name','required');
            $this->form_validation->set_rules('email','Email Id','required|valid_email|is_unique[user.email]');
            $this->form_validation->set_rules('mobile','Mobile Number','required');
            $this->form_validation->set_rules('username','User name','required|is_unique[user.username]');
            $this->form_validation->set_rules('password','Password','required');
            if ($this->form_validation->run()==true) {
                $userdata['fname']=$this->input->post('fname');
                $userdata['lname']=$this->input->post('lname');
                $userdata['email']=$this->input->post('email');
                $userdata['username']=$this->input->post('username');
                $userdata['password']=password_hash($this->input->post('password'), PASSWORD_DEFAULT);
                $userdata['mobile']=$this->input->post('mobile');
                $userdata['register_at']=date("Y-m-d h:i:s");
                $userdata['last_login']=date("Y-m-d h:i:s");
                $userdata['status']=$this->input->post('status');
                $this->load->model('admin/Admin_model');
                if($this->Admin_model->addNewUsers($userdata))
                {
                    $this->session->set_flashdata("add_user_y","User Added Successfully...");
                    // exit();
                }else
                {
                   $this->session->set_flashdata("add_user_n","User Not Added Somthing Wrong...");
                    // exit();
                }
                return redirect('admin/dashboard');
            }else{
                //form error accurs
                $this->form_validation->set_error_delimiters('<p class="error invalid-feedback">','</p>');
                 $this->load->view('admin/useradd');   
            }

        }else
        {
           return redirect('admin/dashboard');
        }
       
    }
    function updateUser()
    {
        if (isset($_POST['updateUser'])) {
            // echo "inform";
            // exit();
            // Form Validation For New User added by Admin
            $this->form_validation->set_rules('fname','First Name','required');
            $this->form_validation->set_rules('lname','Last name','required');
            $this->form_validation->set_rules('email','Email Id','required');
            $this->form_validation->set_rules('mobile','Mobile Number','required');
            $this->form_validation->set_rules('username','User name','required');
            // $this->form_validation->set_rules('password','Password','required');
            if ($this->form_validation->run()==true) {
                
                $update_id=$this->input->post('uid');
                $edit_userdata['fname']=$this->input->post('fname');
                $edit_userdata['lname']=$this->input->post('lname');
                $edit_userdata['email']=$this->input->post('email');
                $edit_userdata['username']=$this->input->post('username');
                $edit_userdata['password']=password_hash($this->input->post('password'), PASSWORD_DEFAULT);
                $edit_userdata['mobile']=$this->input->post('mobile');
                $edit_userdata['updated_at']=date("Y-m-d h:i:s");
                $edit_userdata['status']=$this->input->post('status');
                
                $this->load->model('admin/Admin_model');
                if($this->Admin_model->updateUsers($update_id,$edit_userdata))
                {
                   
                    // exit();
                    $this->session->set_flashdata("update_user_y","User Updated Successfully...");
                    // exit();
                }else
                {
                    // exit();
                   $this->session->set_flashdata("update_user_n","User Not Updated Somthing Wrong...");
                    // exit();
                }
                return redirect('admin/dashboard');
            }else{
                //form error accurs
                $this->form_validation->set_error_delimiters('<p class="error invalid-feedback">','</p>');
                 // $this->load->view('admin/useradd');
                $id=$this->input->post('uid');
                // $_POST['role']="edit";
                // $this->load->view('admin/useradd',$_POST);
                 return redirect('admin/dashboard/edituser/'.$id);    
            }

        }else
        {
           return redirect('admin/dashboard');
        }
       
    }
    function edituser($id)
    {   

        $this->load->model('admin/Admin_model');
       $data= $this->Admin_model->getUserById($id);

       $data[0]["role"]="edit";
       $data[0]['page']="adduser";
       $data[0]['usercount']=$this->Admin_model->getUsersCount();
        $this->load->view('admin/useradd',$data[0]);
        // exit();

    }
    function deleteuser($id)
    {   
        echo "$id delete user";
            // exit();
        $this->load->model('admin/Admin_model');
        if($this->Admin_model->deleteUsers($id))
        {
            $this->session->set_flashdata("del_user_y","User Deleted Successfully...");

        }else{
            $this->session->set_flashdata("del_user_y","Somthing Wrong...");
        }
        return redirect('admin/dashboard');


    }   
      function logout()
    {
       $this->session->unset_userdata('aid');
       $this->session->unset_userdata('auname');
       echo "<br>";
       return redirect('admin/admin');

    }
}

?>