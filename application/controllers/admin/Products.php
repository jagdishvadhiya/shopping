<?php 

/**
 * summary
 */
class Products extends CI_Controller
{
    /**
     * summary
     */
        public function __construct()
        {
            parent::__construct();
            if(!$this->session->userdata('aid'))
            {
               return redirect('admin/login');
           }
       }
       public function index()
       {
           $this->load->model('admin/Admin_model');
           $data=$this->Admin_model->getProductData();

           $dashboard['productdata']=$data;
           $dashboard['orders_count']=$this->returncount('orders');
           $dashboard['products_count']=$this->returncount('products');
           $dashboard['category_count']=$this->returncount('category');
           $dashboard['users_count']=$this->returncount('users');
           $dashboard['subcategory_count']=$this->returncount('subcategory');
           $dashboard['productreviews_count']=$this->returncount('productreviews');
           $this->load->view('admin/products',$dashboard);
       }
       public function productadd()
       {
        if(isset($_POST['addNewProduct']))
        {
            $cat_select=$this->input->post('category');
            $sub_cat_select=$this->input->post('subcategory');
            $this->form_validation->set_rules('pro_name','Product Name','required');
            $this->form_validation->set_rules('pro_price','Product Price','required');
            $this->form_validation->set_rules('pro_qty','Product Quentity','required');
            $this->form_validation->set_rules('pro_description','Product Discription','required');
            $this->form_validation->set_rules('category','Category','required');
            $this->form_validation->set_rules('subcategory','Sub Category','required');
            
            if($this->form_validation->run()==true && $cat_select!=0 && $sub_cat_select!=0)
            {
                // echo "<pre>";
                // print_r($_POST);
                // print_r($_FILES);
                // exit();
               // file uploading
                    echo '<pre>';
                $data = array();

                  // Count total files
                  $countfiles = count($_FILES['files']['name']);
             
                  // Looping all files
                  for($i=0;$i<$countfiles;$i++){
             
                    if(!empty($_FILES['files']['name'][$i])){
             
                      // Define new $_FILES array - $_FILES['file']
                      $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                      $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                      $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                      $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                      $_FILES['file']['size'] = $_FILES['files']['size'][$i];

                      // Set preference
                      $config['upload_path'] = 'public/pro_img/'; 
                      $config['allowed_types'] = 'jpg|jpeg|png|gif';
                      $config['max_size'] = '10000'; // max_size in kb
                      $config['file_name'] = $_FILES['files']['name'][$i];
                      $config['encrypt_name'] = TRUE;
             
                      //Load upload library
                      $this->load->library('upload',$config); 
             
                      // File upload
                      if($this->upload->do_upload('file')){
                        // Get data about the file
                        $uploadData = $this->upload->data();
                        // print_r($uploadData);
                        $filename = $uploadData['file_name'];

                        // Initialize array
                        $data['filenames'][] = $filename;
                      }else{
                         $data['errors'][] = $this->upload->display_errors();
                      }
                    }
             
                  }
                   if(empty($data['errors']))
                     {
                        if(!empty($data['filenames']))
                        {
                            print_r($data['filenames']);
                            // exit();
                            $productdata['category']=$this->input->post('category');
                            $productdata['subCategory']=$this->input->post('subcategory');
                            $productdata['name']=$this->input->post('pro_name');
                            $productdata['price']=$this->input->post('pro_price');
                            $productdata['description']=$this->input->post('pro_description');
                            $productdata['qty']=$this->input->post('pro_qty');
                            $productdata['status']=$this->input->post('status');
                            $productdata['postingDate']=date("Y-m-d h:i:s");
                            $productdata['extraimages']='';
                            $icount=1;
                            foreach ($data['filenames'] as $key => $value) {
                                if($icount<=3){
                                    $productdata['image'.$icount]=$data['filenames'][$key];
                                }else{
                                 $productdata['extraimages'].=$data['filenames'][$key].'||';

                             }
                                 $icount+=1;

                            }
                            $productdata['extraimages']= rtrim($productdata['extraimages'], "||");
                            $this->load->model('admin/Admin_model');
                            if($this->Admin_model->addNewProduct($productdata))
                            {
                                $this->session->set_flashdata('add_product_y','Product Added Successfully...');
                            }else{
                                $this->session->set_flashdata('add_product_n','Product Not Added...');

                            }
                            return redirect('admin/products');
                        
                           
                        }
                     }
                  $this->form_validation->set_error_delimiters('<p class="error invalid-feedback">','</p>');
                  $dashboard['orders_count']=$this->returncount('orders');
                  $dashboard['products_count']=$this->returncount('products');
                  $dashboard['category_count']=$this->returncount('category');
                  $dashboard['users_count']=$this->returncount('users');
                  $dashboard['subcategory_count']=$this->returncount('subcategory');
                  $dashboard['productreviews_count']=$this->returncount('productreviews');
                 
                   $dashboard['file_msg']=$data;
                  
                  $this->load->view('admin/productadd',$dashboard);

            }else{
               
               
              $this->form_validation->set_error_delimiters('<p class="error invalid-feedback">','</p>');
              $dashboard['orders_count']=$this->returncount('orders');
              $dashboard['products_count']=$this->returncount('products');
              $dashboard['category_count']=$this->returncount('category');
              $dashboard['users_count']=$this->returncount('users');
              $dashboard['subcategory_count']=$this->returncount('subcategory');
              $dashboard['productreviews_count']=$this->returncount('productreviews');
              $this->load->view('admin/productadd',$dashboard);
            }
        }else {

          $dashboard['orders_count']=$this->returncount('orders');
          $dashboard['products_count']=$this->returncount('products');
          $dashboard['category_count']=$this->returncount('category');
          $dashboard['users_count']=$this->returncount('users');
          $dashboard['subcategory_count']=$this->returncount('subcategory');
          $dashboard['productreviews_count']=$this->returncount('productreviews');
          $this->load->view('admin/productadd',$dashboard);
            // return redirect('admin/products');
        }
    }
    // edit product
    public function editproduct($id=0)
       {

          if(isset($_POST['editProduct']))
          {
             
          
                  $pid=$this->input->post("pro_id");
                  $productdata=array();
                  echo "<pre>";
                  print_r($_POST);
                  print_r($_FILES);
                  if($_FILES['files']['error'][0]!=0)
                  {
                            $productdata['category']=$this->input->post('category');
                            $productdata['subCategory']=$this->input->post('subcategory');
                            $productdata['name']=$this->input->post('pro_name');
                            $productdata['price']=$this->input->post('pro_price');
                            $productdata['description']=$this->input->post('pro_description');
                            $productdata['qty']=$this->input->post('pro_qty');
                            $productdata['status']=$this->input->post('status');
                            $productdata['updationDate']=date("Y-m-d h:i:s");
                             $this->load->model('admin/Admin_model');
                            if($this->Admin_model->updateProduct($pid,$productdata))
                            {
                              $this->session->set_flashdata("update_product_y",'Product Updated Successfully...');
                              return redirect('admin/products');
                            }else
                            {
                              $this->session->set_flashdata("update_product_n",'Product Updated Failed...');
                               return redirect('admin/products/editproduct/'.$pid);
                            }
                       
                  }
                  $newImages='';
                 // file uploading
                     

                    // Count total files
                    $countfiles = count($_FILES['files']['name']);
                    $data=array();
                       
                    // Looping all files
                    for($i=0;$i<$countfiles;$i++)
                    {
               
                      if(!empty($_FILES['files']['name'][$i]))
                      {
                       
                                // Define new $_FILES array - $_FILES['file']
                                $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                                $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                                $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                                $_FILES['file']['size'] = $_FILES['files']['size'][$i];

                                // Set preference
                                $config['upload_path'] = 'public/pro_img/'; 
                                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                $config['max_size'] = '10000'; // max_size in kb
                                $config['file_name'] = $_FILES['files']['name'][$i];
                                $config['encrypt_name'] = TRUE;
                       
                                //Load upload library
                                $this->load->library('upload',$config); 
                       
                                // File upload
                                if($this->upload->do_upload('file'))
                                {
                                  // Get data about the file
                                  $uploadData = $this->upload->data();
                                  // print_r($uploadData);
                                  $filename = $uploadData['file_name'];

                                  // Initialize array
                                  $data['filenames'][] = $filename;
                                }else{
                                   $data['errors'][] = $this->upload->display_errors();
                                }
                      }else
                      {
                        $data['errors'][]="somthing wrong";
                      }
               
                    }
                     if(empty($data['errors']))
                       {
                          if(!empty($data['filenames']))
                          {
                             
                              // exit();
                      
                              foreach ($data['filenames'] as $key => $value) 
                              {
                                  
                                   $newImages.='||'.$data['filenames'][$key];

                               }
                                  
                          }
                        }
                       
                             
                          
                             
                         
                       
                  echo "<pre>";
                  print_r($data);
                  if(!isset($data['errors']))
                  {
                            $productdata['category']=$this->input->post('category');
                            $productdata['subCategory']=$this->input->post('subcategory');
                            $productdata['name']=$this->input->post('pro_name');
                            $productdata['price']=$this->input->post('pro_price');
                            $productdata['description']=$this->input->post('pro_description');
                            $productdata['qty']=$this->input->post('pro_qty');
                            $productdata['status']=$this->input->post('status');
                            $productdata['updationDate']=date("Y-m-d h:i:s");
                            if(!empty($newImages))
                            {
                              $productdata['extraimages']=$newImages;

                            }
                            $this->load->model('admin/Admin_model');
                            if($this->Admin_model->updateProduct($pid,$productdata))
                            {
                              $this->session->set_flashdata("update_product_y",'Product Updated Successfully...');
                              return redirect('admin/products');
                            }else
                            {
                              $this->session->set_flashdata("update_product_n",'Product Updated Failed...');
                               return redirect('admin/products/editproduct/'.$pid);
                            }
                         

                  }else{
                    $this->session->set_flashdata("img_eror",'Wrong Image choose another');
                    return redirect('admin/products/editproduct/'.$pid);
                  }
              }
              else
              {
                    if($id==0)
                    {
                        return redirect('admin/products');
                    }else
                    {
                               $this->load->model('admin/Admin_model');
                         $data=$this->Admin_model->getDataById('products',$id);
                         // echo "<pre>";
                         // print_r($data[0]);
                         // exit();
                         $dashboard['productdata']=$data[0];
                         $dashboard['orders_count']=$this->returncount('orders');
                         $dashboard['products_count']=$this->returncount('products');
                         $dashboard['category_count']=$this->returncount('category');
                         $dashboard['users_count']=$this->returncount('users');
                         $dashboard['subcategory_count']=$this->returncount('subcategory');
                         $dashboard['productreviews_count']=$this->returncount('productreviews');

                         $this->load->view('admin/productedit',$dashboard);
                    }
              }
              // return redirect('admin/products');
              
    }

    //delete products
    public function deleteproduct($id=0)
    {
        if($id==0){
            return redirect('admin/products');
        }else {
            $this->load->model('admin/Admin_model');
            if($this->Admin_model->deleteProduct($id))
            {
                $this->session->set_flashdata("delete_product_y","Product Deleted Successfully...");
            }else{
                $this->session->set_flashdata("delete_product_n","Product Not Deleted !!!");

            }
           return redirect('admin/products');
        }
    }

    function getSubCategory()
    {
        $cat_id = $this->input->post('cat_id');

    // echo json_encode($cat_id); 
    // load model 
    $this->load->model('admin/Admin_model');
    
    // get data 
    $data = $this->Admin_model->getSubcategory($cat_id);
   echo json_encode($data);
    }

    function returncount($table)
    {
        $this->load->model('admin/Admin_model');
        $data=$this->Admin_model->getCountByTable($table);
        return count($data);

    }

    // ajax delete image by button click
    function deleteImageAjax()
    {
     $pro_id=$this->input->post('pro_id'); 
     $image_name=$this->input->post('image_name'); 
     $array=explode('==',$image_name);
    $column_name=$array[0];
    $value=$array[1];

     $this->load->model('admin/Admin_model');
    $response=array();
    if($this->Admin_model->deleteImage($pro_id,$column_name,$value))
    {

      $response['status']=1;
    }else{
      $response['status']=0;
    }
    echo json_encode($response);

     //    echo "<pre>";
     // print_r($data);
    }
}

?>