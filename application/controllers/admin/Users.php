<?php 

class Users extends CI_Controller
{
    /**
     * summary
     */
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('aid') && !$this->session->userdata('auname'))
       {
        return redirect('admin/login');

       }
    }
    function index()
    {
        $this->load->model('admin/Admin_model');
        $data=$this->Admin_model->getDataByTable('users');
         $dashboard['userdata']=$data;
         $dashboard['orders_count']=$this->returncount('orders');
        $dashboard['products_count']=$this->returncount('products');
        $dashboard['category_count']=$this->returncount('category');
        $dashboard['users_count']=$this->returncount('users');
        $dashboard['subcategory_count']=$this->returncount('subcategory');
        $dashboard['productreviews_count']=$this->returncount('productreviews');
       $this->load->view('admin/users',$dashboard);
    }
    function useradd()
    {
       
       if(isset($_POST['addNewUser']))
        {
               $pass=$this->input->post('password');
               $cpass=$this->input->post('cpassword');
                  // Form Validation For New User added by Admin
            $this->form_validation->set_rules('fullname','Full Name','required');
            $this->form_validation->set_rules('email','Email Id','required|valid_email|is_unique[users.email]');
            // $this->form_validation->set_rules('mobile','Mobile Number','required');
            $this->form_validation->set_rules('mobile', 'Mobile Number ', 'required|regex_match[/^[0-9]{10}$/]');
            $this->form_validation->set_rules('password','Password','required');
            $this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|matches[password]');
            if ($this->form_validation->run()==true)
            {
                $userdata['name']=$this->input->post('fullname');
                $userdata['email']=$this->input->post('email');
                $userdata['contactno']=$this->input->post('mobile');
                $userdata['password']=password_hash($this->input->post('password'), PASSWORD_DEFAULT);
                $userdata['regDate']=date("Y-m-d h:i:s");
                $userdata['status']=$this->input->post('status');
                print_r($_POST);
                $this->load->model('admin/Admin_model');
                if($this->Admin_model->addNewUsers($userdata))
                {
                    $this->session->set_flashdata("add_user_y","User Added Successfully...");
                    // exit();
                }else
                {
                   $this->session->set_flashdata("add_user_n","User Not Added Somthing Wrong...");
                    // exit();
                }
                return redirect('admin/dashboard');
            }else{
                //form error accurs
                
                $this->form_validation->set_error_delimiters('<p class="error invalid-feedback">','</p>');
                $dashboard['orders_count']=$this->returncount('orders');
                $dashboard['products_count']=$this->returncount('products');
                $dashboard['category_count']=$this->returncount('category');
                $dashboard['users_count']=$this->returncount('users');
                $dashboard['subcategory_count']=$this->returncount('subcategory');
                $dashboard['productreviews_count']=$this->returncount('productreviews');
               $this->load->view('admin/useradd',$dashboard);

            }
        }else{

            $dashboard['orders_count']=$this->returncount('orders');
            $dashboard['products_count']=$this->returncount('products');
            $dashboard['category_count']=$this->returncount('category');
            $dashboard['users_count']=$this->returncount('users');
            $dashboard['subcategory_count']=$this->returncount('subcategory');
            $dashboard['productreviews_count']=$this->returncount('productreviews');
            $this->load->view('admin/useradd',$dashboard);
        }
     
    }
    function editUser($id)
    {
            $this->load->model('admin/Admin_model');
            $data=$this->Admin_model->getUserById($id);

            $dashboard['edituserdata']=$data[0];
            $dashboard['orders_count']=$this->returncount('orders');
            $dashboard['products_count']=$this->returncount('products');
            $dashboard['category_count']=$this->returncount('category');
            $dashboard['users_count']=$this->returncount('users');
            $dashboard['subcategory_count']=$this->returncount('subcategory');
            $dashboard['productreviews_count']=$this->returncount('productreviews');
            $this->load->view('admin/edituser',$dashboard);
    }
    function updateUser()
    {
       
       if(isset($_POST['updateUser']))
        {
                $uid=$this->input->post('uid');
                  // Form Validation For New User added by Admin
            $this->form_validation->set_rules('fullname','Full Name','required');
            $this->form_validation->set_rules('email','Email Id','required|valid_email');
            // $this->form_validation->set_rules('mobile','Mobile Number','required');
               $this->form_validation->set_rules('mobile', 'Mobile Number ', 'required|regex_match[/^[0-9]{10}$/]');
            // $this->form_validation->set_rules('password','Password','required');
           $pas=$this->input->post('password');
           $cpas=$this->input->post('cpassword');
           if($pas!==$cpas){

            $this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|matches[password]');
           }
            if ($this->form_validation->run()==true)
            {
                $userdata['name']=$this->input->post('fullname');
                $userdata['email']=$this->input->post('email');
                $userdata['contactno']=$this->input->post('mobile');
                $userdata['updationDate']=date("Y-m-d h:i:s");
                if(!empty($pass) && !empty($cpass))
                {
                    $userdata['password']=password_hash($this->input->post('password'), PASSWORD_DEFAULT);
                }
                $userdata['status']=$this->input->post('status');
                echo '<pre>';
                print_r($userdata);
                $this->load->model('admin/Admin_model');
                if($this->Admin_model->updateUsers($uid,$userdata))
                {
                    $this->session->set_flashdata("add_user_y","User Updated Successfully...");
                    // exit();
                }else
                {
                   $this->session->set_flashdata("add_user_n","User Not Updated Somthing Wrong...");
                    // exit();
                }
                return redirect('admin/dashboard');
            }else{
                //form error accurs
                
                $this->form_validation->set_error_delimiters('<p class="error invalid-feedback">','</p>');
                $dashboard['orders_count']=$this->returncount('orders');
                $dashboard['products_count']=$this->returncount('products');
                $dashboard['category_count']=$this->returncount('category');
                $dashboard['users_count']=$this->returncount('users');
                $dashboard['subcategory_count']=$this->returncount('subcategory');
                $dashboard['productreviews_count']=$this->returncount('productreviews');
               $this->editUser($uid);

            }
        }else{

            $dashboard['orders_count']=$this->returncount('orders');
            $dashboard['products_count']=$this->returncount('products');
            $dashboard['category_count']=$this->returncount('category');
            $dashboard['users_count']=$this->returncount('users');
            $dashboard['subcategory_count']=$this->returncount('subcategory');
            $dashboard['productreviews_count']=$this->returncount('productreviews');
            $this->load->view('admin/dashboard',$dashboard);
        }
     
    }
    function returncount($table)
    {
        $this->load->model('admin/Admin_model');
        $data=$this->Admin_model->getCountByTable($table);
        return count($data);
        
    }
    function deleteUser($id)
    {
        $this->load->model('admin/Admin_model');
        if($this->Admin_model->deleteUser($id))
                {
                    $this->session->set_flashdata("del_user_y","User Deleted Successfully...");
                    // exit();
                }else
                {
                   $this->session->set_flashdata("del_user_n","User Not Deleted Somthing Wrong...");
                    // exit();
                }
                return redirect('admin/Users');
    }

}