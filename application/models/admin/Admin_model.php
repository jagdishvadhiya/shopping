<?php 

/**
 * summary
 */
class Admin_model extends CI_Model
{
    /**
     * summary
     */
    public function __construct()
    {
        parent::__construct();
    }
    //login check
    public function getAdminByEmail($email)
    {
        return $this->db->where('email',$email)->get('admin')->row();
    }
    // get data from any table
    public function getDataByTable($table)
    {
         return $this->db->get($table)->result_array();
    }
    // data count from table
    public function getCountByTable($table)
    {
        return $this->db->get($table)->result_array();
    }
    //get data by id from any table
    public function getDataById($table,$id)
    {
        return $this->db->where('id',$id)->get($table)->result_array();
    }
    // users controler
    public function getUsers()
    {
        return $this->db->get('users')->result_array();
    }
    public function addNewUsers($dataArray)
    {
        $this->db->insert('users',$dataArray);
        return $this->db->insert_id();
    }
     public function getUserById($id)
    {
        return $this->db->where('id',$id)->get('users')->result_array();
    }
     public function updateUsers($id,$dataArray)
    {
        
        $this->db->where('id', $id);
        return $this->db->update('users',$dataArray); 
    }
    public function deleteUser($id)
    {
       return $this->db->where('id',$id)->delete('users');
    }

    // category model start here...
    public function addNewCategory($dataArray)
    {
        $this->db->insert('category',$dataArray);
        return $this->db->insert_id();
    }
    // update category
     public function updateCategory($id,$dataArray)
    {
        
        $this->db->where('id', $id);
        return $this->db->update('category',$dataArray); 
    }
    // delete category
    public function deleteCat($id)
    {
        $this->db->where('categoryid',$id)->delete('subcategory');
        return $this->db->where('id',$id)->delete('category');

    }

   //sub category start here
    //get category name by id
    public function getCategoryNameById($id)
    {
        return $this->db->where('id',$id)->get('category')->row();
    }
    // add new sub category
    public function addNewSubCategory($dataArray)
    {
        $this->db->insert('subcategory',$dataArray);
        return $this->db->insert_id();
    }
    //update sub category
     public function updateSubCategory($id,$dataArray)
    {
        
        $this->db->where('id', $id);
        return $this->db->update('subcategory',$dataArray); 
    }
     // delete category
    public function deleteSubCat($id)
    {
       return $this->db->where('id',$id)->delete('subcategory');
    }
    //get sub category by id
    public function getSubCategoryNameById($id)
    {
        return $this->db->where('id',$id)->get('subcategory')->row();
    }

    //product fetch from database
    function getProductData()
    {
        $prodata=$this->getDataByTable('products');
        foreach ($prodata as $key => $value) {
            $cat_data=$this->getCategoryNameById($value['category']);
            $prodata[$key]['categoryName']= $cat_data->categoryName;
              $cat_data=$this->getSubCategoryNameById($value['subCategory']);
            $prodata[$key]['subcategoryName']= $cat_data->subcategory;

        }


        return $prodata;
    }
    // for dynamic data get subcategory
    function getSubcategory($cat_id)
    {

        $response = array();
     
        // Select record
        $this->db->select('id,subcategory');
        $this->db->where('categoryid', $cat_id);
        $q = $this->db->get('subcategory');
        $response = $q->result_array();

        return $response;
    }
      // product work start here
      // insert product into table
      
    public function addNewProduct($dataArray)
    {
        $this->db->insert('products',$dataArray);
        return $this->db->insert_id();
    }
    // delete product from table
    public function deleteProduct($id)
    {
        echo "<pre>";
        $prodata=$this->getDataById('products',$id);
        $images=array();
        if($prodata[0]['image1']!="")
        {
            $images[]=$prodata[0]['image1'];
        }
        if($prodata[0]['image2']!="")
        {
            $images[]=$prodata[0]['image2'];
        }
        if($prodata[0]['image3']!="")
        {
            $images[]=$prodata[0]['image3'];
        }
        if($prodata[0]['extraimages']!="")
        {
            $extraimages=$prodata[0]['extraimages'];
            $allImgs=explode('||', $extraimages);
            foreach($allImgs as $key)
            {
                $images[]=$key;
            }
        }
        // foreach($images as $oneimg){
        //         echo base_url().'public/pro_img/'.$oneimg.'<br>';
        //         // unlink(base_url().'public/pro_img/'.$oneimg);
        //     }
        //     exit();
        if($this->db->where('id',$id)->delete('products'))
        {
            foreach($images as $oneimg){
                // echo base_url().'public/pro_img/'.$oneimg;
                unlink("public/pro_img/".$oneimg);
                // unlink(base_url().'public/pro_img/'.$oneimg);
            }
            return true;
        }else{
            return false;
        }
        
       

    }
    //delete image by ajax call form database
    public function deleteImage($pid,$column_name,$value)
    {
        $this->load->helper("file");

      
        if($column_name!="extraimages")
        { $this->db->where('id', $pid);
            $this->db->update("products", array($column_name=>''));
                        
                 delete_files(base_url().'public/pro_img/'.$value);
                 return true;
            
        }else{
            $this->db->select("extraimages");
            $this->db->where("id",$pid);
            $result=$this->db->get('products')->result_array();
           $images=$result[0]['extraimages'];
           $newImg=str_replace($value, "", $images);
           $newImg=str_replace("||||", "||", $newImg);
           $newImg=trim($newImg,"||");
          
            $this->db->where('id', $pid);
            $this->db->update("products", array("extraimages"=>$newImg));
            delete_files(base_url().'public/pro_img/'.$value);
            return true;

        }
    }
    // update product
    public function updateProduct($pid,$dataArray)
    {
        $imagesdata= $this->db->select('extraimages')->where('id',$pid)->get('products')->row();
        if(!empty($imagesdata->extraimages))
        {
            $dataArray['extraimages']=$imagesdata->extraimages.$dataArray['extraimages'];
        }else{
             $dataArray['extraimages']=trim($dataArray['extraimages'],'||');
        }
        if($this->db->where('id',$pid)->update("products",$dataArray))
        {
            return true;
        }else{
            return false;
        }
    }

}

 ?>