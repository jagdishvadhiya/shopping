<?php $this->load->view('admin/header'); ?>
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-4">
      </div><!-- /.col -->
      <div class="col-sm-4">
        <h4 class="m-0 text-danger bg-success text-center">
           <?= $this->session->flashdata('add_user_y'); ?>

        </h4>
        <h4 class="m-0 text-danger bg-danger text-center">
           <?= $this->session->flashdata('add_user_n'); ?>
        </h4>
      </div><!-- /.col -->
      <div class="col-sm-4">
        
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        

          <!-- ./col -->
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3><?= $products_count; ?></h3>

                <h2>Products</h2>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="<?php echo base_url('admin/Products'); ?>" class="small-box-footer">View All<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <a href="<?= base_url().'admin/users' ?>">
            <div class="small-box bg-warning text-white">
              <div class="inner">
                <h3 class=""><?= $users_count; ?></h3>

                <h2>Users</h2>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="<?php echo base_url('admin/users'); ?>" class="small-box-footer">View All <i class="fas fa-arrow-circle-right"></i></a>
            </div>
            </a>
          </div>
          <!-- ./col -->
           <div class="col-lg-4 col-6">
            <!-- small box -->
             <a href="<?= base_url().'admin/categorys' ?>">
            <div class="small-box bg-secondary">
              <div class="inner">
                <h3><?= $category_count; ?></h3>

                <h2>Categorys</h2>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="<?php echo base_url('admin/categorys'); ?>" class="small-box-footer">View All <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </a>
          </div>
          
          <!-- ./col -->
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-primary">
              <div class="inner">
                <h3><?= $subcategory_count; ?></h3>

                <h2>Sub Categorys</h2>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="<?php echo base_url('admin/subcategorys'); ?>" class="small-box-footer">View All<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
       
          <!-- ./col -->
          
          <!-- ./col -->
        </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>

<!-- model for add new user -->



<!-- end model for add new user -->

<?php $this->load->view('admin/footer'); ?>