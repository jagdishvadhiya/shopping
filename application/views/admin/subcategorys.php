<?php $this->load->view('admin/header'); ?>
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-4">
        <h1 class="m-0 text-dark">Dashboard</h1>
      </div><!-- /.col -->
      <div class="col-sm-4">
       <h5 class="m-0 text-danger bg-success text-center">
         <?= $this->session->flashdata('add_sub_category_y'); ?>
         <?= $this->session->flashdata('update_sub_category_y'); ?>
         <?= $this->session->flashdata('delete_subcategory_y'); ?>

       </h5>
       <h5 class="m-0 text-danger bg-danger text-center">
         <?= $this->session->flashdata('add_sub_category_n'); ?>
         <?= $this->session->flashdata('update_sub_category_n'); ?>
         <?= $this->session->flashdata('delete_subcategory_n'); ?>
         <?= $this->session->flashdata('editerror'); ?>
       </h5>
     </div><!-- /.col -->
     <div class="col-sm-4">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="<?php echo base_url().'admin/dashboard' ?>">Dashboard</a></li>
        <li class="breadcrumb-item active">Sub Categorys</li>
      </ol>
    </div><!-- /.col -->
  </div><!-- /.row -->
</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Sub Categorys</h3>
        <a class="btn float-right btn-rounded py-0 p-xl-1 btn-primary" href="<?= base_url().'admin/subcategorys/subcategoryadd'; ?>"><i class="fas fa-plus"></i>&nbsp; AddSubCategory</a>

        <!-- </div> -->
      </div>
      <!-- /.card-header -->
      <div class="card-body ">

         <table id="datatable" class="utable table table-responsive table-bordered table-striped text-center table-responsive ">
          <thead>
            <tr>
              <th>Id</th>
              <th>Category</th>
              <th>Sub Category</th>

              <th>Regioster Date</th>
              <th>Update Date</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>

            <?php 
            if(!empty($subcategorydata)){
              foreach ($subcategorydata as $key => $value) {
                ?>
                <tr>
                  <td><?= $value["id"] ?></td>
                  <td><?= $value["categoryName"] ?></td>
                  <td><?= $value["subcategory"] ?></td>
                  <td><?= $value["creationDate"] ?></td>
                 
                  <td><?= (!empty($value["updationDate"]))?$value["updationDate"]:'-' ?></td>
                  <td><?php 
                  echo ($value['status'] == 1)?'<span class="badge badge-success">Active</span>':'<span class="badge badge-danger">Deavtive</span>';
                        // echo '<span class="badge badge-success">'.$value['status'].'</span>'; ?>

                      </td>
                      <td><a href="<?php echo base_url()."admin/subcategorys/editsubcategory/{$value['id']}"; ?>"><i class="fas fa-user-edit text-primary" ></i></a>&nbsp;<a href="<?php echo base_url()."admin/subcategorys/deletesubcategory/{$value['id']}"; ?>"><i class="fas fa-user-times text-danger"></i></a></td>
                    </tr>

                    <?php
                  } }else{
                    echo '<tr>
                              <td colspan="9"><h3>NO Data Found</h3></td>
                          </tr>';
                  }

                  ?>
                  
                </tbody>
      
              </table>

          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>

    <!-- model for add new user -->

    

    <!-- end model for add new user -->

    <?php $this->load->view('admin/footer'); ?>