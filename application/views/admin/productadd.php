<?php $this->load->view('admin/header'); ?>
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-4">
        <h1 class="m-0 text-dark">Dashboard</h1>
      </div><!-- /.col -->
      <div class="col-sm-4">
        <h4 class="m-0 text-danger bg-success text-center"><?= $this->session->flashdata('del_user_y'); ?></h4>
        <h4 class="m-0 text-danger bg-danger text-center"><?= $this->session->flashdata('del_user_n'); ?></h4>
      </div><!-- /.col -->
      <div class="col-sm-4">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url().'admin/dashboard' ?>">Dashboard</a></li>

          <li class="breadcrumb-item active">Add New Product</li>

        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content m-3 bg-white">
  <div class="container-fluid col-10 ">
    <!-- Small boxes (Stat box) -->
    <div class="card bg-light">

      <!-- /.card-header -->
      <!-- card body starts -->
      <div class="card-body ">

       <form action="<?= base_url().'admin/products/productadd'; ?>" method="post" id="addNewCategory" enctype="multipart/form-data">
        <!-- form start -->
        

        <!-- first name input -->
        <div class="form-group">
          <!-- set dynamic lass for html bootstrap error show validation -->

          <label for="cat_name">Product Name</label>
          <input type="text" name="pro_name" class="form-control <?= (form_error('pro_name')!="")?'is-invalid':"" ?>" aria-describedby="firstName-error" aria-invalid="true" value="<?= set_value('pro_name','');?>" placeholder="Enter Product Name">
          <?= (form_error('pro_name')!="")?form_error('pro_name'):"" ?>

        </div>
        <div class="row">
          <div class="col">
            <div class="form-group">
              <label for="selectCategory">Category</label>
              <select class="form-control <?= (form_error('category')!="")?'is-invalid':"" ?>" name="category" id="selectCategory">
                <option value="">Select Category</option>
                <?php 
                //load categorys
                
                $this->load->model('admin/Admin_model');
                $allcategorys=$this->Admin_model->getDataByTable('category');
                foreach ($allcategorys as $key => $value) { 
                  ?>
                  <option value="<?= $value['id'] ?>" <?=  set_select('category', $value['id'],($value['id']=='category')?TRUE:FALSE) ?>><?= $value['categoryName'] ?></option>
                  <?php
                }
                ?>
              </select>
              <?= (form_error('category')!="")?form_error('category'):"" ?>
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <?php 
              if(!empty(set_value('subcategory')))
              {
                echo "<input type='hidden' class='d-none' id='selectedSub_cat' value=".set_value('subcategory')." >";
              }else{
                echo "<input type='hidden' class='d-none' id='selectedSub_cat' value='0'>";

              }
              ?>
              <label for="selectSubCategory">Sub Category</label>
              <select class="form-control  <?= (form_error('subcategory')!="")?'is-invalid':"" ?>"  name="subcategory" id="selectSubCategory">
                <option value="">Select Sub Category</option>

                <!-- <option value="">Select Sub Category</option> -->

              </select>
              <?= (form_error('subcategory')!="")?form_error('subcategory'):"" ?>

            </div>

          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="row">
              <div class="col">
                <div class="form-group">

                  <label for="cat_name">Product Price</label>
                  <input type="number" min="1" name="pro_price" class="form-control <?= (form_error('pro_price')!="")?'is-invalid':"" ?>" aria-describedby="firstName-error" aria-invalid="true" value="<?= set_value('pro_price','');?>" placeholder="Enter Product Price">
                  <?= (form_error('pro_price')!="")?form_error('pro_price'):"" ?>

                </div>
              </div>
            </div>
            <div class="row">
             <div class="col">
              <div class="form-group">

                <label for="cat_name">Product Quentity</label>
                <input type="number" min="1" name="pro_qty" class="form-control <?= (form_error('pro_qty')!="")?'is-invalid':"" ?>" aria-describedby="firstName-error" aria-invalid="true" value="<?= set_value('pro_qty','');?>" placeholder="Enter Product Quentity">
                <?= (form_error('pro_qty')!="")?form_error('pro_qty'):"" ?>

              </div>
            </div>
          </div>

        </div>
        <div class="col">
          <div class="form-group">
            <label for="categoryDescription">Description</label>
            <textarea name="pro_description" class="form-control  <?= (form_error('pro_description')!="")?'is-invalid':"" ?>" placeholder="Enter Product Description Here..." id="categoryDescription" rows="5"><?= set_value('pro_description'); ?></textarea>
            <?= (form_error('pro_description')!="")?form_error('pro_description'):"" ?>
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="custom-file">
          <input type="file" name="files[]" multiple class="custom-file-input" id="proImage" class="<?= (form_error('files')!="")?'is-invalid':"" ?>">
          <label class="custom-file-label" for="customFile">Upload Images</label>
        </div>


      </div>
      <?php 
      if(isset($file_msg))
      {
       if(isset($file_msg['errors']))
       {
        echo '<div class="row">';
        foreach($file_msg['errors'] as $erors)
        {
          echo ' <div class="col-12"><p class="text-danger">'.$erors.'</p></div>';
        }
        echo  '</div>';

      }else{
        if(isset($file_msg['success']))
      {
        echo "<p class='text-success'>".$file_msg['success'][0]." </p>";
      }
      }
    }

      ?>
      


      <!-- status select -->
      <div class="form-group">
        <label for="selectStatus">Status</label>
        <select class="form-control" name="status" id="selectStatus">
          <option value="1">Active</option>
          <option value="0">Deactive</option>
        </select>
      </div>


      <div class="form-group">
        <div class="row my-5">
          <div class="col-4"></div>
          <div class="col-2">
            <input type="submit" name="addNewProduct" class="btn btn-primary btn-block float-right">

          </div>
          <div class="col-2"> 
            <input type="reset" class="btn btn-warning btn-block float-left">
          </div>
          <div class="col-4"></div>

        </div>
      </div>




    </form>


    <!-- card body end -->
  </div>

</div>

</div><!-- /.container-fluid -->
</section>

<?php $this->load->view('admin/footer'); ?>
<!-- <script>

  $(document).ready(function(){

  var selectedSub_cat=$('#selectedSub_cat').val();
  alert('a'+selectedSub_cat);
});
</script> -->