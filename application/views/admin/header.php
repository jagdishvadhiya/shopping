
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JK Shop | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url().'public/assets/'?>plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?= base_url().'public/assets/'?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url().'public/assets/'?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?= base_url().'public/assets/'?>plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url().'public/assets/'?>dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url().'public/assets/'?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?= base_url().'public/assets/'?>plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?= base_url().'public/assets/'?>plugins/summernote/summernote-bs4.css">
  <!-- custom css -->
  <link rel="stylesheet" href="<?= base_url().'public/assets/'?>plugins/custom_css.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.21/datatables.min.css"/>
  <script>
    var base_url = '<?php echo base_url(); ?>';
 //   console.log( base_url );
</script>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>

      </ul>

      <!-- SEARCH FORM -->


      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Messages Dropdown Menu -->
        <h4 class="mt-1 text-primary"><?php echo "<span class='text-dark'>Welcome</span> ".ucfirst($this->session->userdata("auname")); ?></h4>
        <li class="nav-item">

          <a class="nav-link" href="<?= base_url().'admin/dashboard/logout'; ?>" role="button">
            Logout
          </a>

        </li>
      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="index3.html" class="brand-link text-center bg-white">

        <span class="brand-text font-weight-light"><strong style="font-size:20px">JK Shop</strong></span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->


        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->

           <li class="nav-item has-treeview menu-open ">
            <a href="<?= base_url().'admin/users' ?>" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>Users<span class="badge badge-danger right"><?= $users_count; ?></span>
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview menu-open ">
            <a href="<?= base_url().'admin/products' ?>" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>Products<span class="badge badge-danger right"><?= $products_count; ?></span>
              </p>
            </a>
          </li>

          

          <li class="nav-item has-treeview menu-open ">
            <a href="<?= base_url().'admin/categorys' ?>" class="nav-link">
              <i class="nav-icon fas fa-store"></i>
              <p>Categorys<span class="badge badge-danger right"><?= $category_count; ?></span>
              </p>
            </a>
          </li>

          <li class="nav-item has-treeview menu-open ">
            <a href="<?= base_url().'admin/subcategorys' ?>" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>Sub Categorys<span class="badge badge-danger right"><?= $subcategory_count; ?></span>
              </p>
            </a>
          </li>
          


        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
