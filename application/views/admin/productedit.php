
<?php $this->load->view('admin/header'); ?>
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-4">
        <h1 class="m-0 text-dark">Dashboard</h1>
      </div><!-- /.col -->
      <div class="col-sm-4">
          <h4 class="m-0 text-danger bg-danger text-center">
       <p class="text-danger"><?php echo $this->session->flashdata("img_eror"); ?></p>
       <p class="text-danger"><?php echo $this->session->flashdata("update_product_n"); ?></p>
        </h4>
           
      </div><!-- /.col -->
      <div class="col-sm-4">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url().'admin/dashboard' ?>">Dashboard</a></li>

          <li class="breadcrumb-item active">Edit Product</li>

        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content m-3 bg-white">
  <div class="container-fluid col-10 ">
    <!-- Small boxes (Stat box) -->
    <div class="card bg-light">

      <!-- /.card-header -->
      <!-- card body starts -->
      <div class="card-body ">

       <form action="<?= base_url().'admin/products/editproduct'; ?>" method="post" id="editCategory" enctype="multipart/form-data">
        <!-- form start -->
        

        <!-- first name input -->
        <div class="form-group">
          <!-- set dynamic lass for html bootstrap error show validation -->
          <input type="hidden" name="pro_id" value="<?php if($productdata!=""){echo $productdata['id'];}else{echo "";} ?>" id="pro_id">
          <label for="cat_name">Product Name</label>
          <input type="text" name="pro_name" class="form-control <?= (form_error('pro_name')!="")?'is-invalid':"" ?>" aria-describedby="firstName-error" aria-invalid="true" value="<?= set_value('pro_name',($productdata!="")?$productdata['name']:'');?>" placeholder="Enter Product Name" required>
          <?= (form_error('pro_name')!="")?form_error('pro_name'):"" ?>

        </div>
        <div class="row">
          <div class="col">
            <div class="form-group">
              <?php if($productdata!=""){$cat_default=$productdata['category'];}; ?>
              
              <label for="selectCategory">Category</label>
              <select class="form-control <?= (form_error('category')!="")?'is-invalid':"" ?>" name="category" id="selectCategory" required>
                <option value="">Select Category</option>
                <?php 
                //load categorys
                
                $this->load->model('admin/Admin_model');
                $allcategorys=$this->Admin_model->getDataByTable('category');
                foreach ($allcategorys as $key => $value) { 
                  ?>
                  <option value="<?= $value['id'] ?>" <?=  set_select('category', $value['id'],($value['id']=='category' ||$value['id']== $cat_default)?TRUE:FALSE) ?>><?= $value['categoryName'] ?></option>
                  <?php
                }
                ?>
              </select>
              <?= (form_error('category')!="")?form_error('category'):"" ?>
            </div>
          </div>
          <div class="col">
            <div class="form-group">
              <?php 
              if($productdata!=""){$sub_cat_default=$productdata['subCategory'];}; 
              if(!empty(set_value('subcategory')))
              {
                echo "<input type='hidden' class='d-none' id='selectedSub_cat' value=".set_value('subcategory')." >";
              }else{
                echo "<input type='hidden' class='d-none' id='selectedSub_cat' value='0'>";

              }
              ?>
              <label for="selectSubCategory">Sub Category</label>
              <select class="form-control  <?= (form_error('subcategory')!="")?'is-invalid':"" ?>"  name="subcategory" id="selectSubCategory" required>
               <!-- default selected sub category -->
               <?php 
                //load categorys

               $this->load->model('admin/Admin_model');
               $allsubcategorys=$this->Admin_model->getSubcategory($cat_default);
               foreach ($allsubcategorys as $key => $value) { 
                ?>
                <option value="<?= $value['id'] ?>" <?=  set_select('subcategory', $value['id'],($value['id']=='subcategory' ||$value['id']== $productdata['subCategory'])?TRUE:FALSE) ?>><?= $value['subcategory'] ?></option>
                <?php
              }
              ?>



            </select>
            <?= (form_error('subcategory')!="")?form_error('subcategory'):"" ?>

          </div>

        </div>
      </div>
      <div class="row">
        <div class="col">
          <div class="row">
            <div class="col">
              <div class="form-group">

                <label for="cat_name">Product Price</label>
                <input type="text" name="pro_price" required class="form-control <?= (form_error('pro_price')!="")?'is-invalid':"" ?>" aria-describedby="firstName-error" aria-invalid="true" value="<?= set_value('pro_price',$productdata['price']);?>" placeholder="Enter Product Price">
                <?= (form_error('pro_price')!="")?form_error('pro_price'):"" ?>

              </div>
            </div>
          </div>
          <div class="row">
           <div class="col">
            <div class="form-group">

              <label for="cat_name">Product Quentity</label>
              <input type="number" name="pro_qty" class="form-control <?= (form_error('pro_qty')!="")?'is-invalid':"" ?>" aria-describedby="firstName-error" aria-invalid="true" value="<?= set_value('pro_qty',$productdata['qty']);?>" placeholder="Enter Product Quentity" required>
              <?= (form_error('pro_qty')!="")?form_error('pro_qty'):"" ?>

            </div>
          </div>
        </div>

      </div>
      <div class="col">
        <div class="form-group">
          <label for="categoryDescription">Description</label>
          <textarea name="pro_description" class="form-control  <?= (form_error('pro_description')!="")?'is-invalid':"" ?>" placeholder="Enter Product Description Here..." id="categoryDescription" rows="5"><?= set_value('pro_description',$productdata['description']); ?></textarea>
          <?= (form_error('pro_description')!="")?form_error('pro_description'):"" ?>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label for="productImage">Product Images</label>
      <div class="row">
        <!-- show all product images -->
        <?php
        $allimages=array();
        if(!empty($productdata['image1'])){ array_push($allimages, $productdata['image1']); }
        if(!empty($productdata['image2'])){array_push($allimages, $productdata['image2']); }
        if(!empty($productdata['image3'])){ array_push($allimages, $productdata['image3']); }
        if(!empty($productdata['extraimages'])){

         $extraimages=$productdata['extraimages'];
         $extraimage_array=explode("||",$extraimages);
         foreach($extraimage_array as $oneimage)
         {
          array_push($allimages, $oneimage);
        }
      }
      foreach($allimages as $key => $image){
        $image_data='';
        if($key==0)
        {
          $image_data='image1=='.$image;
        }elseif($key==1)
        {
          $image_data='image2=='.$image;
        }elseif($key==2)
        {
          $image_data='image3=='.$image;
        }else{
          $image_data='extraimages=='.$image;
        }
        ?>
        <div class="col-3">
         <div class="card" >
           <button type="button" class="btn close " 
           data-dismiss="alert" aria-label="Close" data-image="<?php echo $image_data ?>" id="deleteImage"> 

           <span aria-hidden="true" class="float-right mr-2 mt-2 text-danger"><i class="far fa-times-circle "></i></span> 
         </button> 
         <img src="<?php echo base_url().'public/pro_img/'.$image; ?>"  class="card-img-top image-responsive" width="150" height="150" alt="Image of ">

       </div>
     </div>

     <?php
   }

   ?>


   
   


 </div>

          <!-- div class="col-3 my-1 ">
            <img src="<?php echo base_url() ?>public/pro_img/12b674173358b9fed97d7a1441656ae1.jpg" 
             class="img-responsive border border-dark" 
             width="200" height="200" />
          </div>
        -->

      </div>
      <div class="custom-file">
        <input type="file" name="files[]" multiple class="custom-file-input" id="proImage" class="<?= (form_error('files')!="")?'is-invalid':"" ?>">
        <label class="custom-file-label" for="customFile">Upload Images</label>
      </div>
        <p class="text-danger"><?php echo $this->session->flashdata("img_eror"); ?></p>


    </div>



  <!-- status select -->
  <div class="form-group">
    <label for="selectStatus">Status</label>
    <select class="form-control" name="status" id="selectStatus">
      <option value="1">Active</option>
      <option value="0">Deactive</option>
    </select>
  </div>


  <div class="form-group">
    <div class="row my-5">
      <div class="col-4"></div>
      <div class="col-2">
        <input type="submit" name="editProduct" class="btn btn-primary btn-block float-right">

      </div>
      <div class="col-2"> 
        <input type="reset" class="btn btn-warning btn-block float-left">
      </div>
      <div class="col-4"></div>

    </div>
  </div>




</form>


<!-- card body end -->
</div>

</div>

</div><!-- /.container-fluid -->
</section>

<?php $this->load->view('admin/footer'); ?>
