<?php $this->load->view('admin/header'); ?>
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-4">
        <h1 class="m-0 text-dark">Dashboard</h1>
      </div><!-- /.col -->
      <div class="col-sm-4">

      </div><!-- /.col -->
      <div class="col-sm-4">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url().'admin/dashboard' ?>">Dashboard</a></li>

          <li class="breadcrumb-item active">Edit Sub Category</li>

        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content mx-3 bg-white">
  <div class="container-fluid col-10">
    <!-- Small boxes (Stat box) -->
    <div class="card bg-light">
      
      <!-- /.card-header -->
      <!-- card body starts -->
      <div class="card-body ">
       <form action="<?= base_url().'admin/subcategorys/subcategoryupdate'; ?>" method="post" id="addNewCategory">
        <!-- form start -->
        
        <?php 
        if(!empty($editsubcategorydata))
        {
          // print_r($editsubcategorydata);
          // exit();
          $r=1;

        }else{
          $r=0;
          $this->session->set_flashdata('editerror','Somthing Wrong Please try Again...');
          return redirect('admin/subcategorys');
        }

        ?>
        <!-- first name input -->
        <div class="form-group">
          <!-- set dynamic lass for html bootstrap error show validation -->

          <label for="cat_name">Sub Category Name</label>
          <input type="hidden" name="id" value="<?= $editsubcategorydata['id']; ?>">
          <input type="text" name="sub_cat_name" class="form-control <?= (form_error('sub_cat_name')!="")?'is-invalid':"" ?>" aria-describedby="firstName-error" aria-invalid="true" value="<?= set_value('sub_cat_name',($r==1)?$editsubcategorydata["subcategory"]:'')?>" placeholder="Enter Full Name">
          <?= (form_error('sub_cat_name')!="")?form_error('sub_cat_name'):"" ?>

        </div>
        <div class="form-group">
          <label for="selectStatus">Category</label>
          <select class="form-control <?= ( $this->session->flashdata('selectcat')!="")?'is-invalid':"" ?>" name="category" id="selectStatus">
            <option value="0">Select Category</option>
            <?php 
                //load categorys
            $this->load->model('admin/Admin_model');
            $allcategorys=$this->Admin_model->getDataByTable('category');
            foreach ($allcategorys as $key => $value) { 
              ?>
              <option value="<?= $value['id'] ?>" <?= ($value['id']==$editsubcategorydata['categoryid'] )?'selected':''; ?> ><?= $value['categoryName'] ?></option>

              <?php
            }

            ?>



          </select>
            <?= ( $this->session->flashdata('selectcat')!="")?'<p class="invalid-feedback">'.$this->session->flashdata('selectcat').'</p>':"" ?>
        </div>
        

        <!-- status select -->
        <div class="form-group">
          <label for="selectStatus">Status</label>
          <select class="form-control" name="status" id="selectStatus">
            <option value="1">Active</option>
            <option value="0">Deactive</option>
          </select>
        </div>


        <div class="form-group">
          <div class="row my-5">
            <div class="col-4"></div>
            <div class="col-2">
              <input type="submit" name="updateSubCategory" class="btn btn-primary btn-block float-right">

            </div>
            <div class="col-2"> 
              <input type="reset" class="btn btn-warning btn-block float-left">
            </div>
            <div class="col-4"></div>

          </div>
        </div>




      </form>


      <!-- card body end -->
    </div>

  </div>

</div><!-- /.container-fluid -->
</section>

<?php $this->load->view('admin/footer'); ?>