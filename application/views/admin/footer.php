   <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="text-center">
    <strong>Copyright &copy; <?= date("Y"); ?><a href="<?= base_url() ?>admin/dashboard">Jk Shop</a>.</strong>
    All rights reserved.
    </div>
   
  </footer>

</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?= base_url().'public/assets/'?>plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?= base_url().'public/assets/'?>plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?= base_url().'public/assets/'?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?= base_url().'public/assets/'?>plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?= base_url().'public/assets/'?>plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="<?= base_url().'public/assets/'?>plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?= base_url().'public/assets/'?>plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?= base_url().'public/assets/'?>plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?= base_url().'public/assets/'?>plugins/moment/moment.min.js"></script>
<script src="<?= base_url().'public/assets/'?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?= base_url().'public/assets/'?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?= base_url().'public/assets/'?>plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?= base_url().'public/assets/'?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url().'public/assets/'?>dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?= base_url().'public/assets/'?>dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url().'public/assets/'?>dist/js/demo.js"></script>
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.21/datatables.min.js"></script><script>

  $(document).ready( function () {
    $('#datatable').DataTable();
} );

</script>
<script src="<?= base_url().'public/assets/'?>dist/js/custom_js.js"></script>

</body>
</html>
