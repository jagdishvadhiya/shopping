
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Ecom | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url().'public/assets/'?>plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?= base_url().'public/assets/'?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url().'public/assets/'?>dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page bg-info" >
  
<div class="login-box">
  <div class="login-logo">
    <a href="<?= base_url().'admin/login'; ?>" class="text-white"><b>Admin </b>Login</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <!-- <p class="login-box-msg text-danger"><?= $this->session->flashdata('invalid'); ?></p> -->

      <form action="<?= base_url().'admin/login/' ?>" method="post">
        <div class="input-group mb-3">
          <input type="email" name="email" class="form-control <?= (form_error('email'))?'is-invalid':''; ?>" placeholder="Email" value='<?= set_value("email"); ?>'>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>

          </div>
        </div>
          <p class="text-danger text-center"><?= $this->session->flashdata('invalidemail'); ?></p>
          <?= form_error('email'); ?>
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control <?= (form_error('password'))?'is-invalid':'' ?>" placeholder="Password" value='<?= set_value("password"); ?>'>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
         <p class="text-danger text-center"><?= $this->session->flashdata('invalidpassword'); ?></p>
                  <?= form_error('password'); ?>

        <div class="row">
          
          <!-- /.col -->
          <div class="col">
            <button type="submit" class="btn btn-primary btn-block" name="adminLogin">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <!-- /.social-auth-links -->

     
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?= base_url().'public/assets/'?>plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url().'public/assets/'?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url().'public/assets/'?>dist/js/adminlte.min.js"></script>

</body>
</html>
