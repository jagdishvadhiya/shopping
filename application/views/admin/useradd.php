<?php $this->load->view('admin/header'); ?>
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-4">
        <h1 class="m-0 text-dark">Dashboard</h1>
      </div><!-- /.col -->
      <div class="col-sm-4">
        <h4 class="m-0 text-danger bg-success text-center"><?= $this->session->flashdata('del_user_y'); ?></h4>
        <h4 class="m-0 text-danger bg-danger text-center"><?= $this->session->flashdata('del_user_n'); ?></h4>
      </div><!-- /.col -->
      <div class="col-sm-4">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url().'admin/dashboard' ?>">Dashboard</a></li>

          <li class="breadcrumb-item active">Add New User</li>

        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content mx-3 bg-white">
  <div class="container-fluid col-10">
    <!-- Small boxes (Stat box) -->
    <div class="card bg-light">

      <!-- /.card-header -->
      <!-- card body starts -->
      <div class="card-body ">
       <form action="<?= base_url().'admin/users/useradd'; ?>" method="post" id="addNewuser">
        <!-- form start -->
        

        <!-- first name input -->
        <div class="form-group">
          <!-- set dynamic lass for html bootstrap error show validation -->

          <label for="FullName">Full Name</label>
          <input type="text" name="fullname" class="form-control <?= (form_error('fullname')!="")?'is-invalid':"" ?>" aria-describedby="firstName-error" aria-invalid="true" value="<?= set_value('fullname','');?>" placeholder="Enter Full Name">
                      <?= (form_error('fullname')!="")?form_error('fullname'):"" ?>

        </div>
        <!-- Last name input -->
        <!-- <div class="form-group">
          <label for="LastName">Last Name</label>
          <input type="text" name="lname" class="form-control " value="" id="lastName" placeholder="Enter Last Name">
        </div> -->

        <!-- Email Id input -->
        <div class="form-group">
          <label for="EmailId">Email Id</label>
          <input type="text" name="email" class="form-control <?= (form_error('email')!="")?'is-invalid':"" ?>" value="<?= set_value('email','');?>" id="emailId" placeholder="Enter email Id">
                      <?= (form_error('email')!="")?form_error('email'):"" ?>

        </div>
        <!-- Mobile Number input -->
        <div class="form-group">
          <label for="MobileNumber">Mobile Number</label>
          <input type="phone" name="mobile" class="form-control <?= (form_error('mobile')!="")?'is-invalid':"" ?>" value="<?= set_value('mobile','');?>" id="mobileNumber" placeholder="Enter mobile Number">
                      <?= (form_error('mobile')!="")?form_error('mobile'):"" ?>

        </div>
        <!-- User name input -->
        <!-- <div class="form-group">
          <label for="UserName">User Name</label>
          <input type="text" name="username" class="form-control " value="" id="UserName" placeholder="Enter User Name">
        </div> -->
        <!-- Password name input -->

        <div class="form-group">

          <label for="PasswordName">Password </label>
          <input type="password" name="password" class="form-control  <?= (form_error('password')!="")?'is-invalid':"" ?>" value="<?= set_value('password','');?>"  id="passwordName" placeholder="Enter Password ">
            <?= (form_error('password')!="")?form_error('password'):"" ?>
           
        </div>
        <div class="form-group">

          <label for="ConPasswordName">Confirm Password Name</label>
          <input type="password" name="cpassword" class="form-control  <?= (form_error('cpassword')!="")?'is-invalid':"" ?>" value="" id="conPasswordName" placeholder="Enter Password ">
           <?= (form_error('cpassword')!="")?'<p class="error invalid-feedback">Pssword Not Matech</p>':"" ?>
        </div>
        <!-- status select -->
        <div class="form-group">
          <label for="selectStatus">Status</label>
          <select class="form-control" name="status" id="selectStatus">
            <option value="1">Active</option>
            <option value="0">Deactive</option>
          </select>
        </div>


      <div class="form-group">
        <div class="row my-5">
          <div class="col-4"></div>
          <div class="col-2">
            <input type="submit" name="addNewUser" class="btn btn-primary btn-block float-right">

          </div>
          <div class="col-2"> 
            <input type="reset" class="btn btn-warning btn-block float-left">
          </div>
          <div class="col-4"></div>

        </div>
      </div>




    </form>


    <!-- card body end -->
  </div>

</div>

</div><!-- /.container-fluid -->
</section>

<?php $this->load->view('admin/footer'); ?>