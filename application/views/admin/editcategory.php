


<?php $this->load->view('admin/header'); ?>
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-4">
        <h1 class="m-0 text-dark">Dashboard</h1>
      </div><!-- /.col -->
      <div class="col-sm-4">
        <h4 class="m-0 text-danger bg-success text-center"><?= $this->session->flashdata('del_user_y'); ?></h4>
        <h4 class="m-0 text-danger bg-danger text-center"><?= $this->session->flashdata('del_user_n'); ?></h4>
      </div><!-- /.col -->
      <div class="col-sm-4">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url().'admin/dashboard' ?>">Dashboard</a></li>

          <li class="breadcrumb-item active">Add New Category</li>

        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content mx-3 bg-white">
  <div class="container-fluid col-10">
    <!-- Small boxes (Stat box) -->
    <div class="card bg-light">

      <!-- /.card-header -->
      <!-- card body starts -->
      <div class="card-body ">

       <form action="<?= base_url().'admin/categorys/categoryupdate'; ?>" method="post" id="addNewCategory">
        <!-- form start -->
        <?php 
        if(!empty($editcategorydata))
        {
          $r=1;
        
        }else{
          $r=0;
          $this->session->set_flashdata('editerror','Somthing Wrong Please try Again...');
          return redirect('admin/categorys');
        }

        ?>

        <!-- first name input -->
        <div class="form-group">
          <!-- set dynamic lass for html bootstrap error show validation -->

          <label for="cat_name">Category Name</label>
          <input type="hidden" name="cat_id" value="<?= $editcategorydata["id"] ?>">
          <input type="text" name="cat_name" class="form-control <?= (form_error('cat_name')!="")?'is-invalid':"" ?>" aria-describedby="firstName-error" aria-invalid="true" value="<?= set_value('cat_name',($r==1)?$editcategorydata["categoryName"]:'')?>" placeholder="Enter Full Name">
          <?= (form_error('cat_name')!="")?form_error('cat_name'):"" ?>

        </div>

        <div class="form-group">
          <label for="categoryDescription">Description</label>
          <textarea name="cat_description" class="form-control" id="categoryDescription" rows="3" ><?= set_value('cat_description',($r==1)?$editcategorydata['categoryDescription']:'')?></textarea>
        </div>


        <!-- status select -->
        <div class="form-group">
          <label for="selectStatus">Status</label>
          <select class="form-control" name="status" id="selectStatus">
            <?= ($r==1)?($editcategorydata['status']==1)?$select=1:$select=0:'' ?>
            <option value="1" <?= ($select==1)?'selected':'' ?>>Active</option>
            <option value="0" <?= ($select==0)?'selected':'' ?>>Deactive</option>
          </select>
        </div>


        <div class="form-group">
          <div class="row my-5">
            <div class="col-4"></div>
            <div class="col-2">
              <input type="submit" name="updateCategory" class="btn btn-primary btn-block float-right">

            </div>
            <div class="col-2"> 
              <input type="reset" class="btn btn-warning btn-block float-left">
            </div>
            <div class="col-4"></div>

          </div>
        </div>




      </form>


      <!-- card body end -->
    </div>

  </div>

</div><!-- /.container-fluid -->
</section>

<?php $this->load->view('admin/footer'); ?>