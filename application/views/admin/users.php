<?php $this->load->view('admin/header'); ?>
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-4">
        <h1 class="m-0 text-dark">Dashboard</h1>
      </div><!-- /.col -->
      <div class="col-sm-4">
       <h4 class="m-0 text-danger bg-success text-center">
         <?= $this->session->flashdata('del_user_y'); ?>

       </h4>
       <h4 class="m-0 text-danger bg-danger text-center">
         <?= $this->session->flashdata('del_user_n'); ?>
       </h4>
     </div><!-- /.col -->
     <div class="col-sm-4">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="<?php echo base_url().'admin/dashboard' ?>">Dashboard</a></li>
        <li class="breadcrumb-item active">Users</li>
      </ol>
    </div><!-- /.col -->
  </div><!-- /.row -->
</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Users</h3>
        <a class="btn float-right btn-rounded py-0 p-xl-1 btn-primary" href="<?= base_url().'admin/users/useradd'; ?>"><i class="fas fa-user-plus"></i>&nbsp; AddUser</a>

        <!-- </div> -->
      </div>
      <!-- /.card-header -->
      <div class="card-body ">

         <table id="datatable" class="utable table table-responsive table-bordered table-striped text-center table-responsive ">
          <thead>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Email</th>
              <th>Mobile</th>

              <th>Regioster Date</th>
              <th>Update Date</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>

            <?php 
            if(!empty($userdata)){
              foreach ($userdata as $key => $value) {
                ?>
                <tr>
                  <td><?= $value["id"] ?></td>
                  <td><?= $value["name"] ?></td>
                  <td><?= $value["email"] ?></td>
                  <td><?= $value["contactno"] ?></td>
                  <td><?= $value["regDate"] ?></td>
                  <td><?= (!empty($value["updationDate"]))?$value["updationDate"]:'-' ?></td>
                  <td><?php 
                  echo ($value['status'] == 1)?'<span class="badge badge-success">Active</span>':'<span class="badge badge-danger">Deavtive</span>';
                        // echo '<span class="badge badge-success">'.$value['status'].'</span>'; ?>

                      </td>
                      <td><a href="<?php echo base_url()."admin/users/edituser/{$value['id']}"; ?>"><i class="fas fa-user-edit text-primary" ></i></a>&nbsp;<a href="<?php echo base_url()."admin/users/deleteuser/{$value['id']}"; ?>"><i class="fas fa-user-times text-danger"></i></a></td>
                    </tr>

                    <?php
                  } }else{
                    echo '<tr>
                              <td colspan="9"><h3>NO Data Found</h3></td>
                          </tr>';
                  }

                  ?>
                  
                </tbody>
      
              </table>

          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>

    <!-- model for add new user -->

    

    <!-- end model for add new user -->

    <?php $this->load->view('admin/footer'); ?>