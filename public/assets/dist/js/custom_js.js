
  $(document).ready(function(){


// product add category and sub category select dynamicly load
	var selectedSub_cat=$('#selectedSub_cat').val();

	$('#selectCategory').change(function(){

		var cat_id = $(this).val();
		
	      // AJAX request
	      console.log( 'category changed'  + cat_id );
		

	      $.ajax({
		      	url: base_url + "admin/products/getSubCategory",
		      	method: 'post',
		      	data: {cat_id: cat_id},
		      	dataType: 'json',
		      	success: function(response){
		      		console.log( 'got the response');
		      		console.log(response);
		      		// alert(response);
			
					// $('#selectSubCategory').find('option').remove().end();
					$('#selectSubCategory').empty().append('<option>Select Sub Category</option>');
		          // Add options
		          $.each(response,function(index,data){
		          	if(selectedSub_cat!="" && selectedSub_cat==data['id'])
		          	{

		          		$('#selectSubCategory').append('<option value="'+data['id']+'" selected="selected">'+data['subcategory']+'</option>');
		          	}else{
		          		$('#selectSubCategory').append('<option value="'+data['id']+'">'+data['subcategory']+'</option>');
		          	}
		          });
		      },
		      error: function() {
		      	console.log( 'error' );
		      }
		 });
	  });

	// delete image on click close button of image show in edit product



	// document.('#deleteImage').click(function(event) {
	// 	 Act on the event 
	// 	var image=$(this).data('image');
	// 	alert('delete'+image);
	// });
	$(document).on('click','#deleteImage', function() {
    //your stuff
    var image=$(this).data('image');
    var pid=$('#pro_id').val();

	      $.ajax({
		      	url: base_url + "admin/products/deleteImageAjax",
		      	method: 'post',
		      	data: {
		      		image_name: image,
		      		pro_id:pid
		      			},
		       dataType: 'JSON',
		      	success: function(response){
		      		// alert(response);
				if(response.status==1){
					alert(response.status);
					location.reload();
				}else{
					alert(response.status);
				}
		      },
		      error: function() {
		      	console.log( 'error' );
		      }
		 });
		
});


});